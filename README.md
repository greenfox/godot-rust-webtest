# Godot Rust Webtest

The goal of this project it to create a container that will reliably and repeatably compile Godot Rust to WASM.

## Build Docker Image

From this working directory, run:

```bash
docker build -t godot_rust_webtest docker #this last "docker" is the folder name in this dir
```

This will build a new docker container named `godot_rust_webtest`. This name is subject to change.

## Run container

From this working directory, run:

```
docker run -it -v $(pwd):/wd -w /wd godot_rust_webtest
```

This will mount this project into the container.

## Compile Godot Rust WASM

From within the docker container

```
cargo +nightly build --target=wasm32-unknown-emscripten --release
```

The above line fails with the below error. Currently working on this.


## DEBUGGING!

- fixed bug with `.cargo/config.toml`
