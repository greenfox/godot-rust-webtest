
use gdnative::{prelude::*, api::OS};

#[derive(NativeClass)]
#[inherit(Label)]
struct LabelUpdater;

#[methods]
impl LabelUpdater {
    fn new(_owner: &Label) -> Self {
        LabelUpdater
    }

    #[method]
    fn _ready(&self,#[base] owner: TRef<Label>) {
        let t = GodotString::from_str("This text was updated from Rust. If you're seeing this, Rust ran correctly!");
        let adding = if OS::godot_singleton().get_name() == GodotString::from_str("HTML5") {
            "\nHOLY SHIT, WE JUST RAN RUST IN A BROWSER WITH GODOT!"
        } else { "" };
        owner.set_text(t + GodotString::from_str(adding));
        owner.add_color_override("font_color",Color::from_rgb(0.0,1.0,0.0));
        godot_print!("hello, world.")
    }
}

fn init(handle: InitHandle) {
    handle.add_class::<LabelUpdater>();
}

godot_init!(init);
