#!/bin/bash
set -e

export LIB_NAME="godot_rust_webtest"



main()
{
    install_target
    compile
}


install_target()
{
    /root/.cargo/bin/rustup target install ${PLATFORM_LABEL} ${RUSTUP_NIGHTLY}
}

compile()
{
    /root/.cargo/bin/cargo ${CARGO_NIGHTLY} build --target ${PLATFORM_LABEL} ${CARGO_RELEASE_ARGS}
    mkdir -p build
    cp ${TARGET_PATH} build
}


echoVars()
{
    echo PLATFORM_LABEL = ${PLATFORM_LABEL}
    echo CARGO_RELEASE_ARGS = ${CARGO_RELEASE_ARGS}
    echo TARGET_PATH = ${TARGET_PATH}
    echo LIB_NAME = ${LIB_NAME}
}


debugEnv()
{
    export CARGO_RELEASE_ARGS=""
    export TARGET_TYPE_PATH="debug"
    export CARGO_NIGHTLY=""
    export RUSTUP_NIGHTLY=""
}
releaseEnv()
{
    export CARGO_RELEASE_ARGS="--release"
    export TARGET_TYPE_PATH="release"
}
nightlyEnv()
{
    export CARGO_NIGHTLY="+nightly"
    export RUSTUP_NIGHTLY="--toolchain nightly"
}
windowsEnv()
{
    #window-gnu target didn't seem to work on Windows, so lets use msvc there
    #(except when on Linux building for Windows)
    if [[ $OS = Windows_NT ]]; then 
        export PLATFORM_LABEL="x86_64-pc-windows-msvc"
    else
        export PLATFORM_LABEL="x86_64-pc-windows-gnu"
    fi
    export TARGET_PATH="target/${PLATFORM_LABEL}/${TARGET_TYPE_PATH}/${LIB_NAME}.dll"
}
linuxEnv()
{
    export PLATFORM_LABEL="x86_64-unknown-linux-gnu"
    export TARGET_PATH="target/${PLATFORM_LABEL}/${TARGET_TYPE_PATH}/lib${LIB_NAME}.so"
}
javascriptEnv()
{
    export PLATFORM_LABEL="wasm32-unknown-emscripten"
    export TARGET_PATH="target/${PLATFORM_LABEL}/${TARGET_TYPE_PATH}/${LIB_NAME}.wasm"
    nightlyEnv
}

args()
{
    debugEnv

    if [[ $OS = Windows_NT ]]; then
        export TARGET="windows"
    else
        export TARGET="linux"
    fi

    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $1 in
        -e|--editor)
            editor
            return -1
            ;;
        -d|--debug)
            debugEnv
            ;;
        -r|--release)
            releaseEnv
            ;;
        -w|--windows)
            export TARGET="windows"
            ;;
        -l|--linux)
            export TARGET="linux"
            ;;
        -j|--javascript)
            export TARGET="javascript"
            ;;
        -h|--help)
            echoHelp
            return -1
            ;;
        -s|--sleep)
            sleep 1
            ;;
        -e|--everything)
            /root/.cargo/bin/cargo clean
            $0 -j -r
            $0 -w -r
            $0 -l -r
            return -1
            ;;
        *)
            echo "Arguement Error: $1"
            return -1
    esac
    shift
    done


    case $TARGET in
        windows)
            windowsEnv
            ;;
        javascript)
            javascriptEnv
            ;;
        linux)
            linuxEnv
            ;;
        *)
            echo "bad target!"
            return -1
    esac

    
    echoVars
}

echoHelp()
{
cat << EOF
$0 usage:
-e or --editor
    launch editor for this version
-d or --debug
    set build to debug (default)
-r or --release
    set build to release
-w or --windows
    set build to Windows
-l or --linux
    set build to Linux
-h or --help
    print this help
EOF
}

editor()
{
    PROJECT_PATH="godot/project.godot"
    if [[ $OS = Windows_NT ]]; then
        ../.bin/Godot_v3.5-stable_win64.exe ${PROJECT_PATH}
    else
        ../.bin/Godot_v3.5-stable_x11.64 ${PROJECT_PATH}
    fi
}

ORG_PATH=$(pwd)
cd $(dirname $0)

args $@

main


